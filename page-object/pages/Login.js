import {Selector, t} from 'testcafe'

class Login{
    constructor(){
        this.usernameInput = Selector('#login_input-email')
        this.passwordInput = Selector('#login_input-password')
        this.submitButton = Selector('#login_button-login') 
        this.usernameErrorMessage=Selector('main span').withText('Please provide a valid email address.')
        this.passwordErrorMessage=Selector('main span').withText('Please fill in your password.')
        this.passwordUsernameErrorMessage=Selector('#login-form p')
    }
    async loginToApp(username, password){
        if (username != '' && password != '') {
            await t
            .typeText(this.usernameInput, username, {paste:true, replace:true})
            .typeText(this.passwordInput, password, {paste:true, replace:true})
        } 
        
        if (username == '' && password != ''){
            await t.typeText(this.passwordInput, password, {paste:true, replace:true})
        }
 
        if (username != '' && password == ''){
            await t.typeText(this.usernameInput, username, {paste:true, replace:true})
        }
                
}
}
export default Login