import {Selector, t} from 'testcafe'

class Toggle{
    constructor(){
        this.toggleClick = Selector('#login-form div div input')
        this.usernameInput = Selector('#login_input-email')
        this.bussinessLabel = Selector('#login-form div').nth(3).find('label')
        this.emailLabel = Selector('#login-form div').nth(5).find('label')
        this.passwordLabel = Selector('#login-form div').nth(6).find('label')
        this.passwordForget = Selector('#login_link-forgot-password')
        this.creatAccount = Selector('#login-form p')
    }
    }
export default Toggle

