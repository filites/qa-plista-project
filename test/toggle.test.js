import Toggle from '../page-object/pages/Toggle';
const switchLanguage = new Toggle()

fixture `Toggle language button test`
   .page `https://login-test.plista.com/de/`
   .beforeEach( async t => {
    await t
        .wait(1000)
});

test
('Change language to german', async t => {
if(!switchLanguage.toggleClick.checked)
await t
.expect(switchLanguage.bussinessLabel.innerText).eql("Geschäftssitz")
.expect(switchLanguage.emailLabel.innerText).eql("E-Mail-Adresse")
.expect(switchLanguage.passwordLabel.innerText).eql("Passwort")
.expect(switchLanguage.passwordForget.innerText).eql("Passwort vergessen?")
.expect(switchLanguage.creatAccount.innerText).eql("Noch keinen Self Service Account? Hier registrieren")
})

test
('Change language to english', async t => {
if(switchLanguage.toggleClick.checked)
await t
.expect(switchLanguage.bussinessLabel.innerText).eql("Business Location")
.expect(switchLanguage.emailLabel.innerText).eql("E-Mail Address")
.expect(switchLanguage.passwordLabel.innerText).eql("Password")
.expect(switchLanguage.passwordForget.innerText).eql("Forgot your password?")
.expect(switchLanguage.creatAccount.innerText).eql("Don't have a Self Service account? Create one")
})

