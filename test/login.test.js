import Login from '../page-object/pages/Login';

const loginPage = new Login()

fixture `Plista Login E2E Test`
   .page `https://login-test.plista.com/de/`
   .beforeEach( async t => {
    await t
        .wait(1000)
});
test
('Login with username and password', async t => {
loginPage.loginToApp('filaslee@gmail.com','1234')
await t
      .click(loginPage.submitButton)
      .expect(loginPage.passwordUsernameErrorMessage.innerText).contains('Invalid username or password')
})
test
('Login with username and no password', async t => {
loginPage.loginToApp('filaslee@gmail.com', '')
await t
    .click(loginPage.submitButton)
    .expect(loginPage.passwordErrorMessage.innerText).contains('Please fill in your password')
})

test
('Login with no username and password', async t => {
loginPage.loginToApp('', '1235434')
await t
    .click(loginPage.submitButton)
    .expect(loginPage.usernameErrorMessage.innerText).contains('Please provide a valid email address')
})


